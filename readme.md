This is a sample project using
- Spring (spring-context-4.2.4-RELEASE, spring-webmvc-4.2.4-RELEASE)
- Angular Js
- Maven
- Tomcat 

###Webjars
Also, I use webjars libraries to include all required js files.
WebJars is simply taking the concept of a JAR and applying it to client-side libraries or resources. For example, the Angularjs
library may be packaged as a JAR and made available to your Spring MVC application.

WebJars utilizes Maven’s dependency management model to include JavaScript libraries in a project, making it more
accessible to Java developers.

###Spring Java bases Config

WebApplicationInitializer is used configure the ServletContext programmatically in replacement of the WEB-INF/web.xml file.

WebMvcConfigurerAdapter is used to customise the java-based configuration for SpringMVC. It is
as opposed to the mvc-dispatcher.xml. To configure the resources and viewResolver, addResourceHandlers and
getViewResolver are overridden.

### Angular Controllers and js files

app.js file defines the application module configuration and routes.

### How to run this project locally?

- Check out the project
- Run 'mvn clean install'
- mvn tomcat7:run