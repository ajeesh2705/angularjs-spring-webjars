package com.angular.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    public MainController() {
        System.out.println("Creating main controller");
    }

    @RequestMapping("/")
    public String homepage(){
        return "homepage";
    }
}
